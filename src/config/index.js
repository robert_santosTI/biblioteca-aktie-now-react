import axios from "axios";
import { ativaCarregamento, desativaCarregamento } from '../store/actions/config';

import store from '../store/Index'

const axiosInstance = axios.create({
    baseURL: 'HTTP://localhost:82/api'
})

class config {
    api = null
    constructor() {
        this.api = axiosInstance

        // request
        this.api.interceptors.request.use(function (config) {
            config.headers.Authorization = (store.getState().config.token)
            store.dispatch(ativaCarregamento({ url: config.url, data: JSON.stringify(config.data) }))
            return config;
        }, function (error) {
            return Promise.reject(error);
        })

        // response
        this.api.interceptors.response.use(function (response) {

            store.dispatch(desativaCarregamento({ url: response.config.url, data: response.config.data, sucesso: true }))
            return response;
        }, function (error) {
            if (error.response !== undefined) {
                store.dispatch(desativaCarregamento({ url: error.response.config.url, data: error.response.config.data, sucesso: false, msg: error.response.data}))
            } else {
                store.dispatch(desativaCarregamento({ url: error.config.url, data: error.config.data, sucesso: false, msg: error}))
            }
            return Promise.reject(error);
        })
    }


}


export default (new config(axiosInstance))