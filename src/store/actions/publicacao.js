const alteraPublicoes = (publicacoes) => {
    return {
        type: "ALTERA_PUBLICACOES",
        publicacoes
    }
}
const alteraPublicao = (publicacao) => {
    return {
        type: "ALTERA_PUBLICACAO",
        publicacao
    }
}
const abrirCadastroLivro = (valor) => {
    return {
        type: "ABRIR_CADASTRO_LIVRO",
        valor
    }
}
const abrirEditarLivro = (valor) => {
    return {
        type: "ABRIR_EDITAR_LIVRO",
        valor
    }
}


export { alteraPublicoes, abrirCadastroLivro, abrirEditarLivro, alteraPublicao }