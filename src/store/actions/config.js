const alteraLogin = (login) => {
    return {
        type: "ALTERA_LOGIN",
        login
    }
}

const ativaCarregamento = ({ url, data }) => {
    return {
        type: "ATIVA_CARREGAMENTO",
        url,
        data
    }
}


const desativaCarregamento = ({ url, data, sucesso, msg }) => {
    return {
        type: "DESATIVA_CARREGAMENTO",
        url,
        data,
        sucesso,
        msg
    }
}

const removeErro = ({ titulo }) => {
    return {
        type: "REMOVE_ERRO",
        titulo
    }
}

const adicionarSucesso = ({ sucesso }) => {
    return {
        type: "ADICIONAR_SUCESSO",
        sucesso
    }
}

const adicionarToken = ({ token }) => {
    return {
        type: "ADICIONAR_TOKEN",
        token
    }
}

export { alteraLogin, ativaCarregamento, desativaCarregamento, removeErro, adicionarSucesso, adicionarToken }