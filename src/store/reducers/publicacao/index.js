import initial_state from "./state";

export default function publicao(state = initial_state, action) {
    if (action.type === 'ALTERA_PUBLICACOES') {
        return {
            ...state,
            publicacoes:  action.publicacoes
        }

    }if (action.type === 'ALTERA_PUBLICACAO') {
        return {
            ...state,
            publicacao:  action.publicacao
        }

    } else if  (action.type === 'ABRIR_CADASTRO_LIVRO') {
        return {
            ...state,
            abrirCadastroLivro:  action.valor
        }

    } else if  (action.type === 'ABRIR_EDITAR_LIVRO') { 
        return {
            ...state,
            abrirEditarLivro:  action.valor
        }

    } else {
        return state
    }
}

