import initial_state from "./state";

export default function config(state = initial_state, action) {

    if (action.type === 'ATIVA_CARREGAMENTO') {

        return {
            ...state,
            requisicoes: [...state.requisicoes, { url: action.url, data: action.data }]
        }

    } else if (action.type === 'DESATIVA_CARREGAMENTO') {

        let indexReq = state.requisicoes.findIndex(requisicao => requisicao.url === action.url && requisicao.data === action.data)
        if (action.sucesso) {
            return {
                ...state,
                requisicoes: [
                    ...state.requisicoes.splice(0, indexReq),
                    ...state.requisicoes.splice(indexReq + 1)
                ]
            }
        } else {
            return {
                ...state,
                requisicoes: [
                    ...state.requisicoes.splice(0, indexReq),
                    ...state.requisicoes.splice(indexReq + 1)
                ],
                errosRequisicoes: [...state.errosRequisicoes, { msg: action.msg }]
            }

        }

    } else if (action.type === 'ADICIONAR_SUCESSO') {
        return {
            ...state,
            sucesso: action.sucesso 
        }
    }  else if (action.type === 'ADICIONAR_TOKEN') {
        return {
            ...state,
            token: action.token 
        }
    } else {
        return state
    }
}

