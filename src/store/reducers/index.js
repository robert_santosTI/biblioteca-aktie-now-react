import { combineReducers } from "redux";
import   config  from "./config/index";
import   publicacao  from "./publicacao/index";

export default combineReducers({
    config,
    publicacao
})
