import React from 'react';
import { Box, FormControl, InputLabel, Input, Grid, Button } from '@material-ui/core/';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import {CriarUsuarios} from '../../api/usuario'
import { useHistory } from "react-router"
import { adicionarSucesso  } from "../../store/actions/config";
import store from "../../store/Index";


const Login = () => {
    let history = useHistory()

    const [nome, setNome] = React.useState(null)
    const [usuario, setUsuario] = React.useState(null)
    const [senha, setSenha] = React.useState(null)

    const handleUsuario = (event) => {
        setUsuario(event.target.value)
    }

    const handleSenha = (event) => {
        setSenha(event.target.value)
    }
    
    const handleNome = (event) => {
        setNome(event.target.value)
    }
    return (
        <Grid
            container
            justify="center"
            alignItems="center"
            style={{ minHeight: '100vh' }}
        >

            <form>
                <Box>
                    <h1>Biblioteca now </h1>
                </Box>
                <Box>
                    <FormControl fullWidth >
                        <InputLabel htmlFor="nome">Nome</InputLabel>
                        <Input id="nome" value={nome} onChange={handleNome} />
                    </FormControl>
                </Box>
                <Box>
                    <FormControl fullWidth >
                        <InputLabel htmlFor="usuario">Email</InputLabel>
                        <Input id="usuario" value={usuario} onChange={handleUsuario} />
                    </FormControl>
                </Box>
                <Box>
                    <FormControl fullWidth>
                        <InputLabel htmlFor="senha">Senha</InputLabel>
                        <Input id="senha" type="password" value={senha} onChange={handleSenha} />
                    </FormControl>
                </Box>
                <br></br>
                <Box>
                    <FormControl fullWidth>
                        <Button variant="contained" color="primary" onClick={() => { Cadastrar() }}>
                            Cadastrar
                    </Button>
                    </FormControl>
                </Box>
                <br/>
                <Box>
                    <Link to="/login">
                        <FormControl fullWidth>
                            <Button color="default">Voltar</Button>
                        </FormControl>
                    </Link>
                </Box>
            </form>
        </Grid>)



    function Cadastrar() {
        CriarUsuarios({
            email: usuario,
            senha: senha,
            nome: nome
        }).then(()=>{
            store.dispatch(adicionarSucesso({sucesso: 'Cadastro de usuario realizado com sucesso!'}))
            history.push('/login')
        })
    }

}

export default connect(state => ({
    requisicoes: state.config.requisicoes,
    carregando: state.config.requisicoes.length > 0

}))(Login);


