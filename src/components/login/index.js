import React from 'react';
import { Box, FormControl, InputLabel, Input, Grid, Button } from '@material-ui/core/';
import { connect } from 'react-redux'
import { useHistory } from "react-router"

import { Link } from 'react-router-dom'
import { AutenticarUsuarios } from '../../api/usuario'


const Login = () => {

    let history = useHistory()
    const [usuario, setUsuario] = React.useState('')
    const [senha, setSenha] = React.useState('')

    const handleUsuario = (event) => {
        setUsuario(event.target.value)
    }

    const handleSenha = (event) => {
        setSenha(event.target.value)
    }
    return (
        <Grid
            container
            justify="center"
            alignItems="center"
            style={{ minHeight: '100vh' }}
        >

            <form>
                <Box>
                    <h1>Biblioteca now </h1>
                </Box>
                <Box>
                    <FormControl fullWidth >
                        <InputLabel htmlFor="usuario">Email</InputLabel>
                        <Input id="usuario" value={usuario} onChange={handleUsuario} />
                    </FormControl>
                </Box>
                <Box>
                    <FormControl fullWidth>
                        <InputLabel htmlFor="senha">Senha</InputLabel>
                        <Input id="senha" type="password" value={senha} onChange={handleSenha} />
                    </FormControl>
                </Box>
                <br></br>
                <Box>
                    <FormControl fullWidth>
                        <Button variant="contained" color="primary" onClick={() => { autentica() }}>
                            Autenticar
                    </Button>
                    </FormControl>
                </Box>
                <br></br>
                <Box>
                    <Link to="/cadastrese">
                        <FormControl fullWidth>
                            <Button color="default">Cadastre-se</Button>
                        </FormControl>
                    </Link>
                </Box>
            </form>
        </Grid>)



   async function autentica() {
      await  AutenticarUsuarios({
            email: usuario,
            senha: senha
        }).then((res) => {
            history.push("/livros")
        })
    }

}

export default connect(state => ({
    requisicoes: state.config.requisicoes,
    carregando: state.config.requisicoes.length > 0
}))(Login);


