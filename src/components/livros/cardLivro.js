import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle'
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { CadastrarAvaliacao } from "../../api/avaliacaoPublic";
import { BuscarLivros, ExcluirLivroPorID } from "../../api/publicacao";
import EditarLivro from "./edicaoLivro";
import { abrirEditarLivro, alteraPublicao } from "../../store/actions/publicacao";
import store from "../../store/Index";
import AlocarLivro from "./alocarLivro"
import { adicionarSucesso } from "../../store/actions/config";

const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
}));

export default function RecipeReviewCard(props) {
    const classes = useStyles();
    const [openDialog, setOpenDialog] = React.useState(false);
    const [scrollDialog, setScrollDialog] = React.useState('paper');
    const [rating, setRating] = React.useState(props.media);
    const [anchorMenu, setAnchorMenu] = React.useState(null);

    const handleClickMenu = (event) => {
        setAnchorMenu(event.currentTarget);
    };

    const handleCloseMenu = () => {
        setAnchorMenu(null);
    };

    const handleClickOpen = (scrollType) => () => {
        setOpenDialog(true);
        setScrollDialog(scrollType);
    };

    const handleCloseDialog = () => {
        setOpenDialog(false);
    };

    const excluir = () => {
        ExcluirLivroPorID(props.id).then(() => {
            store.dispatch(adicionarSucesso({ sucesso: 'Livro excluido com sucesso!' }))
            handleCloseMenu()
        })
    }
    const editar = () => {
        store.dispatch(alteraPublicao(props))
        store.dispatch(abrirEditarLivro(true))
        handleCloseMenu()

    }

    const handleRatingOnchange = async (event, value, id) => {
        await CadastrarAvaliacao({
            idpublic: id,
            idusuario: 1,
            nota: value
        }).then(async (res) => {
            await BuscarLivros().then((res) => {
                setRating(value)
            })
        }).catch((err) => {
            /* */
        })
    };

    const descriptionElementRef = React.useRef(null);
    React.useEffect(() => {
        if (openDialog) {
            const { current: descriptionElement } = descriptionElementRef;
            if (descriptionElement !== null) {
                descriptionElement.focus();
            }
        }
    }, [openDialog]);

    return (
        <Card className={classes.root}>
            <EditarLivro livro={props} />
            <CardHeader
                avatar={
                    <Avatar aria-label="recipe" className={classes.avatar}>
                        {props.titulo.substring(0, 1)}
                    </Avatar>
                }
                action={
                    <div>
                        <Button aria-controls="long-menu" aria-haspopup="true" onClick={handleClickMenu}>
                            <MoreVertIcon />
                        </Button>
                        <Menu
                            id="long-menu"
                            anchorEl={anchorMenu}
                            keepMounted
                            open={Boolean(anchorMenu)}
                            onClose={handleCloseMenu}
                        >
                            <AlocarLivro onClick={handleCloseMenu} livro={props} usuarios={props.usuarios}></AlocarLivro>
                            <MenuItem onClick={excluir}>Excluir livro</MenuItem>
                            <MenuItem onClick={editar}>Editar livro</MenuItem>
                        </Menu>
                    </div>
                }
                title={props.titulo}
            />
            <CardMedia
                className={classes.media}
                image="/static/images/cards/paella.jpg"
                title="Paella dish"
            />
            <CardContent>
                <Typography variant="body2" color="textSecondary" component="p">
                    {props.descricao.substring(0, 300)}...
                </Typography>
            </CardContent>
            <CardActions disableSpacing>
                <Box component="fieldset" mb={props.id} borderColor="transparent">
                    <Rating
                        name={String(props.id)}
                        value={Number(rating)}
                        onChange={(event, newValue) => {
                            handleRatingOnchange(event, newValue, props.id);
                        }}
                    />
                </Box>
                <IconButton
                    className={clsx(classes.expand, {
                        [classes.expandOpen]: openDialog,
                    })}
                    onClick={handleClickOpen('paper')}
                    aria-expanded={openDialog}
                    aria-label="show more"
                >
                    <ExpandMoreIcon />
                </IconButton>
            </CardActions>
            <Dialog
                open={openDialog}
                onClose={handleCloseDialog}
                scroll={scrollDialog}
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-description"
            >
                <DialogTitle id="scroll-dialog-title">Descrição do livro</DialogTitle>
                <DialogContent dividers={scrollDialog === 'paper'}>
                    <DialogContentText
                        id="scroll-dialog-description"
                        ref={descriptionElementRef}
                        tabIndex={-1}
                    >
                        {props.descricao}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseDialog} color="primary">
                        Sair
                    </Button>
                </DialogActions>
            </Dialog>
        </Card>
    );
}