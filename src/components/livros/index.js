import { connect } from 'react-redux'
import { Component } from 'react'
import Navbar from '../navbar/index'
import {BuscarLivros} from '../../api/publicacao'
import { withStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';

import CardLivro from './cardLivro'

const styler = theme => ({
    root: {
    },
    ContainerSimples: {
        marginLeft: '1rem',
        marginRight: '1rem'
    }
})

class Livros extends Component {
    state = {
        expanded: false,
        handleExpandClick: false,
        usuarios: []
    }
    handleExpandClick = () => {
        this.setState({ expanded: !this.state.expanded })
    }
    componentDidMount() {
        BuscarLivros()
    }
    render(props) {
        const { classes } = this.props;
        return (
            <div>
                <Navbar> </Navbar>
                <div className={classes.ContainerSimples} >
                    <Grid container className={classes.root} spacing={2}>
                        <Grid item xs={12}>
                            <Grid container spacing={2}>
                                {this.props.livros.map(livro => (
                                    <Grid item xs={12} md={4} sm={6} key={livro.id} >
                                        <CardLivro  exemplares={livro.exemplares} titulo={livro.titulo}  disponivelpesquisa={livro.disponivelpesquisa} descricao={livro.descricao} id={livro.id} media={livro.media}></CardLivro>
                                    </Grid>))}
                            </Grid>
                        </Grid>
                    </Grid>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        livros: state.publicacao.publicacoes
    };
};
const mapDispatchToProps = (dispatch) => {
    return {
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styler)(Livros));


