import React, { useEffect } from 'react';
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import store from "../../store/Index";
import Exemplarestable from '../exemplares/exemplares'
import { BuscarLivros, AtualizarLivro } from '../../api/publicacao'

import { abrirEditarLivro } from "../../store/actions/publicacao";
const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(0),
        minWidth: 120,
    },
    appBar: {
        position: 'relative',
        marginBottom: '1rem'
    },
    dialog: {
    },
    contentDialog: {
        paddingLeft: '1rem',
        paddingRight: '1rem'
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1
    },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const EditarLivro = ({ abrirEditarLivrov, publicacao }) => {
    const classes = useStyles();
    const [disponivelpesquisa, setDisponivelPesquisa] = React.useState(publicacao.disponivelpesquisa);
    const [titulo, setTitulo] = React.useState(publicacao.titulo);
    const [descricao, setDescricao] = React.useState(publicacao.descricao);
    const [id, setId] = React.useState(publicacao.id);

    const handleClickDisponivelPesquisa = (event) => {
        setDisponivelPesquisa(event.target.value);
    }

    const handleTitulo = (value) => {
        setTitulo(value.target.value);
    }

    const handleDescricao = (value) => {
        setDescricao(value.target.value);
    }

    const handleClose = () => {
        BuscarLivros()
        store.dispatch(abrirEditarLivro(false))
    }

    const AtualizarLivroU = async () => {
        await AtualizarLivro({ titulo, descricao, disponivelpesquisa, id }).then((res) => {
            BuscarLivros()
            setDisponivelPesquisa(null)
            setTitulo(null)
            setDescricao(null)
            store.dispatch(abrirEditarLivro(false))
        })
    }
    useEffect(() => {
        setTitulo(publicacao.titulo);
        setDescricao(publicacao.descricao);
        setDisponivelPesquisa(publicacao.disponivelpesquisa);
        setId(publicacao.id);
    }, [publicacao]);
    return (
        <div>
            <Dialog className={classes.dialog} fullScreen open={Boolean(abrirEditarLivrov)} onClose={handleClose} TransitionComponent={Transition}>
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                            <CloseIcon />
                        </IconButton>
                        <Typography variant="h6" className={classes.title}>
                            Editar livro
                        </Typography>
                        <Button autoFocus color="inherit" onClick={AtualizarLivroU}>
                            Atualizar
                        </Button>
                    </Toolbar>
                </AppBar>
                <div className={classes.contentDialog}>
                    <Grid container className={classes.root} spacing={2}>
                        <Grid item xs={12} md={6} sm={6} >
                            <TextField
                                required
                                fullWidth
                                id="outlined-required"
                                label="Titulo"
                                variant="outlined"
                                value={titulo}
                                onChange={handleTitulo}
                            />
                        </Grid>
                        <Grid item xs={12} md={6} sm={6} >
                            <FormControl fullWidth variant="outlined" className={classes.formControl}>
                                <InputLabel htmlFor="outlined-age-native-simple">Disponivel pesquisa</InputLabel>
                                <Select
                                    native
                                    fullWidth
                                    value={disponivelpesquisa}
                                    onChange={handleClickDisponivelPesquisa}
                                    label="Disponivel pesquisa"
                                    inputProps={{
                                        name: 'Disponivel pesquisa',
                                        id: 'outlined-age-native-simple',
                                    }}
                                >
                                    <option value={0}>Não</option>
                                    <option value={1}>Sim</option>
                                </Select>
                            </FormControl>
                        </Grid>
                    </Grid>
                    <Grid container className={classes.root} spacing={2}>
                        <Grid item xs={12} md={12} sm={12} >
                            <TextField
                                required
                                fullWidth
                                id="standard-multiline-static"
                                multiline
                                rows={6}
                                label="Descrição"
                                variant="outlined"
                                value={descricao}
                                onChange={handleDescricao}
                            />
                        </Grid>
                    </Grid>
                    <Grid container className={classes.root} spacing={2}>
                        <Grid item xs={12} md={12} sm={12} >
                            <Exemplarestable exemplares={publicacao.exemplares} livro={publicacao}></Exemplarestable>
                        </Grid>
                    </Grid>
                </div>
            </Dialog>
        </div>
    )
}


export default connect((state) => ({
    abrirEditarLivrov: state.publicacao.abrirEditarLivro,
    publicacao: state.publicacao.publicacao
}))(EditarLivro)
