import React from 'react';
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import { abrirCadastroLivro  } from "../../store/actions/publicacao";
import { adicionarSucesso  } from "../../store/actions/config";
import store from "../../store/Index";
import { CriarLivro,BuscarLivros } from '../../api/publicacao'

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(0),
        minWidth: 120,
    },
    appBar: {
        position: 'relative',
        marginBottom: '1rem'
    },
    dialog: {
    },
    contentDialog: {
        paddingLeft: '1rem',
        paddingRight: '1rem'
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const CadastroLivro = ({ CadastroLivroAberto }) => {
    const classes = useStyles();
    const [disponivelpesquisa, setDisponivelPesquisa] = React.useState(false);
    const [titulo, setTitulo] = React.useState();
    const [descricao, setDescricao] = React.useState();

    const handleClickDisponivelPesquisa = (event) => {
        setDisponivelPesquisa(event.target.value);
    };
    const handleTitulo = (value) => {
        setTitulo(value.target.value);
    };
    const handleDescricao = (value) => {
        setDescricao(value.target.value);
    };

    const handleClose = () => {
        store.dispatch(abrirCadastroLivro(false))
    };

    const salvarLivro = async () => {
        await CriarLivro({ titulo, descricao, disponivelpesquisa }).then((res) => {
            BuscarLivros()
            setDisponivelPesquisa(null)
            setTitulo(null)
            setDescricao(null)
            store.dispatch(adicionarSucesso({sucesso: 'Cadastro do livro realizado com sucesso!'}))
            store.dispatch(abrirCadastroLivro(false))
        })
    }

    return (
        <div>
            <Dialog className={classes.dialog} fullScreen open={Boolean(CadastroLivroAberto)} onClose={handleClose} TransitionComponent={Transition}>
                <AppBar className={classes.appBar}>
                    <Toolbar>
                        <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                            <CloseIcon />
                        </IconButton>
                        <Typography variant="h6" className={classes.title}>
                            Cadastrar livro
                        </Typography>
                        <Button autoFocus color="inherit" onClick={salvarLivro}>
                            Salvar
                        </Button>
                    </Toolbar>
                </AppBar>
                <div className={classes.contentDialog}>
                    <Grid container className={classes.root} spacing={2}>
                        <Grid item xs={12} md={6} sm={6} >
                            <TextField
                                required
                                fullWidth
                                id="outlined-required"
                                label="Titulo"
                                variant="outlined"
                                value={titulo}
                                onChange={handleTitulo}
                            />
                        </Grid>
                        <Grid item xs={12} md={6} sm={6} >
                            <FormControl fullWidth variant="outlined" className={classes.formControl}>
                                <InputLabel htmlFor="outlined-age-native-simple">Disponivel pesquisa</InputLabel>
                                <Select
                                    native
                                    fullWidth
                                    value={disponivelpesquisa}
                                    onChange={handleClickDisponivelPesquisa}
                                    label="Disponivel pesquisa"
                                    inputProps={{
                                        name: 'Disponivel pesquisa',
                                        id: 'outlined-age-native-simple',
                                    }}
                                >
                                    <option value={0}>Não</option>
                                    <option value={1}>Sim</option>
                                </Select>
                            </FormControl>
                        </Grid>
                    </Grid>
                    <Grid container className={classes.root} spacing={2}>
                        <Grid item xs={12} md={12} sm={12} >
                            <TextField
                                required
                                fullWidth
                                id="standard-multiline-static"
                                multiline
                                rows={6}
                                label="Descrição"
                                variant="outlined"
                                value={descricao}
                                onChange={handleDescricao}
                            />
                        </Grid>
                    </Grid>
                </div>
            </Dialog>
        </div>
    );
}


export default connect(state => ({
    CadastroLivroAberto: state.publicacao.abrirCadastroLivro
}))(CadastroLivro);


