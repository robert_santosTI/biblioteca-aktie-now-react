import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { BuscarUsuarios } from '../../api/usuario'
import { CriarAlocacao } from '../../api/alocacao'
import { BuscarLivros } from '../../api/publicacao'
import {adicionarSucesso} from '../../store/actions/config'
import store from '../../store/Index'


const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(0),
        minWidth: 120,
    },
    appBar: {
        position: 'relative',
        marginBottom: '1rem'
    },
    dialog: {
    },
    contentDialog: {
        paddingLeft: '1rem',
        paddingRight: '1rem'
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
}));

export default function Cadastrar(props) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [datainicial, setdatainicial] = React.useState();
    const [datafinal, setdatafinal] = React.useState();
    const [usuario, setusuario] = React.useState();
    const [usuarios, setusuarios] = React.useState([]);
    const [idexemplar, setidexemplar] = React.useState();

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleExemplar = (event) => {
        setidexemplar(event.target.value);
    };

    const handleClose = () => {
        setOpen(false);
    };
    const handleUsuario = (event) => {
        setusuario(event.target.value);
    };

    const handleDatainicio = (event) => {
        setdatainicial(event.target.value);
    };
    const handleDatafinal = (event) => {
        setdatafinal(event.target.value);
    };
    const Salvar = async (event) => {
        await CriarAlocacao({
            idpublic: props.livro.id,
            idexemplar: idexemplar,
            idusuario: usuario,
            datainicial: datainicial,
            datafinal: datafinal
        }).then((res) => {
            BuscarLivros()
            store.dispatch(adicionarSucesso({sucesso: 'Alocação realizada com sucesso!'}))
            handleClose()
        })
    };

    useEffect(() => {
        BuscarUsuarios().then((res) => {
            setusuarios(res)
        })
    }, [ ]);

    return (
        <div>
            <MenuItem onClick={handleClickOpen}>Alocar</MenuItem>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title" maxWidth="xs">
                <DialogTitle id="form-dialog-title">Alocar exemplar</DialogTitle>
                <DialogContent>
                    <Grid container className={classes.root} spacing={2}>
                        <Grid item xs={12} md={12} sm={12} >
                            <TextField
                                required
                                fullWidth
                                id="standard-multiline-static"
                                label="Livro"
                                variant="outlined"
                                type="text"
                                value={props.livro.titulo}
                            />
                        </Grid>
                    </Grid>
                    <Grid container className={classes.root} spacing={2}>
                        <Grid item xs={6} md={6} sm={6} >
                            <TextField

                                required
                                fullWidth
                                id="standard-multiline-static"
                                label="Data inicial"
                                variant="outlined"
                                type="date"
                                InputLabelProps={{ shrink: true }}
                                onChange={handleDatainicio}
                                value={datainicial}
                            />
                        </Grid>
                        <Grid item xs={6} md={6} sm={6} >
                            <TextField

                                required
                                fullWidth
                                id="standard-multiline-static"
                                label="Data final"
                                variant="outlined"
                                type="date"
                                InputLabelProps={{ shrink: true }}
                                onChange={handleDatafinal}
                                value={datafinal}
                            />
                        </Grid>
                    </Grid>
                    <Grid container className={classes.root} spacing={2}>
                        <Grid item xs={8} md={8} sm={8} >
                            <FormControl fullWidth variant="outlined" className={classes.formControl}>
                                <InputLabel htmlFor="outlined-age-native-simple">Usuario</InputLabel>
                                <Select
                                    native
                                    value={usuario}
                                    label="Usuario"
                                    onChange={handleUsuario}
                                    inputProps={{
                                        name: 'Usuario',
                                        id: 'outlined-age-native-simple',
                                    }}
                                >
                                    <option></option>
                                    {
                                        usuarios.map(u => (
                                            <option value={u.id} key={u.id}>{u.nome}</option>
                                        ))
                                    }
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={4} md={4} sm={4} >
                            <FormControl fullWidth variant="outlined" className={classes.Exemplar}>
                                <InputLabel htmlFor="outlined-age-native-simple">Exemplar</InputLabel>
                                <Select
                                    native
                                    value={idexemplar}
                                    label="Exemplar"
                                    onChange={handleExemplar}
                                    inputProps={{
                                        name: 'Exemplar',
                                        id: 'outlined-age-native-simple',
                                    }}
                                >
                                    <option></option>
                                    {props.livro.exemplares.map(u => (
                                        u.status === 0 ?
                                            (<option value={u.id} key={u.id}>{u.tombo}</option>)
                                            :
                                            ('')
                                    ))
                                    }
                                </Select>
                            </FormControl>
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancelar
                    </Button>
                    {(datafinal >= datainicial && idexemplar !== null && usuario !== null) ? (
                        <Button color="primary" onClick={Salvar}>
                            Salvar
                        </Button>
                    ) : (
                            ''
                        )
                    }
                </DialogActions>
            </Dialog>
        </div>
    );
}