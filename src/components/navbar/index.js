import React from 'react';
import {useHistory} from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Drawer from '../drawer/index';
import CadastroLivro from '../livros/cadastroLivro'
import { abrirCadastroLivro } from "../../store/actions/publicacao";
import { adicionarToken } from "../../store/actions/config";
import store from "../../store/Index";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginBottom: '5px'
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

export default function Navbar() {
  const classes = useStyles();
  const history= useHistory()
  
  const HandlecloseCadastroLivro = ()=> {
    store.dispatch(abrirCadastroLivro(false))
  }
  const HandleOpenCadastroLivro = ()=> {
    store.dispatch(abrirCadastroLivro(true))
  }
  const HandleLogout = ()=> {
    store.dispatch(adicionarToken(''))
    history.push('/')
  }
  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Drawer></Drawer>
          <Typography variant="h6" className={classes.title}>
            Biblioteca now
          </Typography>
          <Button color="inherit" onClick={HandleOpenCadastroLivro} > Cadastrar</Button>
          <Button color="inherit" onClick={HandleLogout}>Logout</Button>
        </Toolbar>
      </AppBar>
      <CadastroLivro closed={HandlecloseCadastroLivro}/>
    </div>
  );
}