import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import BookmarksIcon from '@material-ui/icons/Bookmarks';
import MeetingRoomTwoToneIcon from '@material-ui/icons/MeetingRoomTwoTone';
import HistoryTwoToneIcon from '@material-ui/icons/HistoryTwoTone';
import MenuIcon from '@material-ui/icons/Menu';
import { Link } from 'react-router-dom'

const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
});

export default function SwipeableTemporaryDrawer() {
  const classes = useStyles();
  const [state, setState] = React.useState({
    aberto: false,
    posicao: 'left'
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setState({ ...state, aberto: open });
  };

  const list = (anchor) => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: anchor === 'top' || anchor === 'bottom',
      })}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        <Link to="/livros">
          <ListItem button>
            <ListItemIcon><BookmarksIcon /></ListItemIcon>
            <ListItemText primary="Livros" />
          </ListItem>
        </Link>
          <ListItem button>
            <ListItemIcon><HistoryTwoToneIcon /></ListItemIcon>
            <ListItemText primary="Historico" secondary="de alocação" />
          </ListItem>
        <ListItem button>
          <ListItemIcon><MeetingRoomTwoToneIcon /></ListItemIcon>
          <ListItemText primary="Sala" secondary="Para estudo" />
        </ListItem>

      </List>
      <Divider />
    </div>
  );

  return (
    <div>
      {
        <React.Fragment >
          <IconButton onClick={toggleDrawer(state.posicao, true)} edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <SwipeableDrawer
            anchor={state.posicao}
            open={state.aberto}
            onClose={toggleDrawer(state.posicao, false)}
            onOpen={toggleDrawer(state.posicao, true)}
          >
            {list(state.posicao)}
          </SwipeableDrawer>
        </React.Fragment>
      }
    </div>
  );
}