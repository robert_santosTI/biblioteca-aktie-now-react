import React, {useEffect} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import { ExcluirExemplares } from '../../api/exemplares'
import { BuscarLivrosPorID } from '../../api/publicacao'
import { alteraPublicao } from "../../store/actions/publicacao";
import store from "../../store/Index";

export default function AlertDialog(props) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  useEffect(() => {
    console.log(props)
}, [props]);

  const handleClose = () => {
    setOpen(false);
  };
  const excluirExemplar = async () => {
      await ExcluirExemplares(props.id )
      await BuscarLivrosPorID(props.idpublic).then((res)=> {
          store.dispatch(alteraPublicao(res[0]))
      })
      setOpen(false);
  };

  return (
    <div>
      
      <Tooltip title="Delete" onClick={handleClickOpen}>
          <IconButton aria-label="delete">
            <DeleteIcon />
          </IconButton>
        </Tooltip>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Deseja realmente deleta o exemplar do id ?"}</DialogTitle>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Não
          </Button>
          <Button onClick={excluirExemplar} color="primary" autoFocus>
            Sim
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}