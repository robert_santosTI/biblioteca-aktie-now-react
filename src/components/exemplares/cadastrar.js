import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import AddBoxTwoToneIcon from '@material-ui/icons/AddBoxTwoTone';
import { CadastrarExemplares } from '../../api/exemplares'
import {  BuscarLivrosPorID } from '../../api/publicacao'
import {  alteraPublicao } from "../../store/actions/publicacao";
import store from "../../store/Index";

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(0),
        minWidth: 120,
    },
    appBar: {
        position: 'relative',
        marginBottom: '1rem'
    },
    dialog: {
    },
    contentDialog: {
        paddingLeft: '1rem',
        paddingRight: '1rem'
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
}));

export default function Cadastrar(props) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [ano, setAno] = React.useState();
    const [edicao, setedicao] = React.useState();
    const [tombo, setTombo] = React.useState();

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    const handleAno = (event) => {
        setAno(event.target.value);
    };
    const handleEdicao = (event) => {
        setedicao(event.target.value);
    };
    const handleTombo = (event) => {
        setTombo(event.target.value);
    };
    const salvarExemplar = async () => {
        console.log(props)
        await CadastrarExemplares({
            ano,
            edicao,
            tombo,
            idpublic: props.livro.livro.id
        });
        setOpen(false);
        await BuscarLivrosPorID(props.livro.livro.id).then((res)=> {
            store.dispatch(alteraPublicao(res[0]))
        })
    };

    return (
        <div>
            <Tooltip title="Filter list" >
                <IconButton aria-label="filter list" onClick={handleClickOpen}>
                    <AddBoxTwoToneIcon />
                </IconButton>
            </Tooltip>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Cadastro exemplar</DialogTitle>
                <DialogContent>
                    <Grid container className={classes.root} spacing={2}>
                        <Grid item xs={12} md={12} sm={12} >
                            <TextField
                                required
                                fullWidth
                                id="standard-multiline-static"
                                label="Ano"
                                variant="outlined"
                                type="number"
                                value={ano}
                                onChange={handleAno}
                            />
                        </Grid>
                        <Grid item xs={12} md={12} sm={12} >
                            <TextField
                                required
                                fullWidth
                                id="standard-multiline-static"
                                label="Edição"
                                variant="outlined"
                                value={edicao}
                                onChange={handleEdicao}
                            />
                        </Grid>
                        <Grid item xs={12} md={12} sm={12} >
                            <TextField
                                required
                                fullWidth
                                id="standard-multiline-static"
                                label="Tombo"
                                type="number"
                                variant="outlined"
                                value={tombo}
                                onChange={handleTombo}
                            />
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={salvarExemplar} color="primary">
                        Salvar
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}