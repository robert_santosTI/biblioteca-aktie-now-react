import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import { adicionarSucesso } from '../store/actions/config'
import store from '../store/Index'
import MuiAlert from '@material-ui/lab/Alert';

export default function PositionedSnackbar(props) {
    const [state, setState] = React.useState({
        open: true,
        vertical: 'bottom',
        horizontal: 'right',
    });

    const { vertical, horizontal, open } = state;

    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
      }
      
    const handleClose = () => {
        store.dispatch(adicionarSucesso({ sucesso: null }))
        setState({ ...state, open: false });
    };


    return (
        <div>
            <Snackbar
                anchorOrigin={{ vertical, horizontal }}
                open={open}
                onClose={handleClose}
                key={vertical + horizontal}
            >
                <Alert severity="success">{props.msg}</Alert>
            </Snackbar>
        </div>
    );
}