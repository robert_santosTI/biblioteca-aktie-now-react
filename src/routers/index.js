import { BrowserRouter, Switch, Route } from 'react-router-dom'
import React, { Component } from 'react';
import { connect } from "react-redux";
import { Backdrop, CircularProgress } from '@material-ui/core/';
import { withStyles } from '@material-ui/styles';
import SnackBarErro from './snackBarErro'
import SnackBarSucess from './snackBarSucess'


import Login from '../components/login/index'
import Home from '../components/home/index'
import Livros from '../components/livros/index'
import CadastreSe from '../components/cadastrese/index'

const Styles = ((theme) => ({
  backdrop: {
    zIndex: 10000,
    color: '#fff',
  },
}));

class Routers extends Component {
  state = {
    open: true
  }

  handleClick = () => {
    this.setState({ open: true });
  };

  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({ open: false });
  };

  render() {
    return (

      <BrowserRouter>
        {
          this.props.errosRequisicoes.map((erro, index) => (
            <SnackBarErro key={index} erro={erro}></SnackBarErro>
          ))
        }
        {
          this.props.sucesso !== null ? (
            <SnackBarSucess msg={this.props.sucesso}></SnackBarSucess>
          ) : ('')
        }


        <Backdrop open={Boolean(this.props.carregando)} className={this.props.classes.backdrop} >
          <CircularProgress color="inherit" />
        </Backdrop>
        <Switch>
          <Route path="/" exact={true} component={Login} />
          <Route path="/login" exact={true} component={Login} />
          <Route path="/home" exact={true} component={Home} />
          <Route path="/livros" exact={true} component={Livros} />
          <Route path="/cadastrese" exact={true} component={CadastreSe} />
        </Switch>
      </ BrowserRouter>

    )
  }
}
const mapStateToProps = state => {
  return {
    carregando: state.config.requisicoes.length,
    errosRequisicoes: state.config.errosRequisicoes,
    sucesso: state.config.sucesso
  };
};
export default connect(mapStateToProps)(withStyles(Styles)(Routers));