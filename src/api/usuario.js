import config from "../config/index";
import { adicionarToken } from '../store/actions/config'
import store from '../store/Index'

export function BuscarUsuarios() {
    return new Promise((resolve, reject) => {
        config.api.get('/usuario').then((res) => {
            resolve(res.data)
        }).catch((err) => {
            reject(err)
        })
    })
}

export function CriarUsuarios(Usuario) {
    return new Promise((resolve, reject) => {
        config.api.post('/usuario', Usuario).then((res) => {
            resolve(res.data)
        }).catch((err) => {
            reject(err)
        })
    })
}

export function AutenticarUsuarios(Usuario) {
    return new Promise((resolve, reject) => {
        config.api.post('/usuario/autenticacao', Usuario).then((res) => {
            store.dispatch(adicionarToken(res.data))
            resolve(res.data)
        }).catch((err) => {
            reject(err)
        })
    })
}

