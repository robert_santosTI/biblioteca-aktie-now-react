import config from "../config/index";

export function CadastrarExemplares(exemplar) {
    return new Promise((resolve, reject) => {
        config.api.put('/exemplares', exemplar).then((res) => {
            resolve(res.data)
        }).catch((err) => {
            reject(err)
        })
    })
}

export function ExcluirExemplares(id) {
    return new Promise ((resolve, reject) => {
        config.api.delete(`/exemplares/${id}`).then(async (res) => {
            resolve(res.data)
        }).catch((err) => {
            reject(err)
        })
    })
}