import config from "../config/index";

export function BuscarAlocacao() {
    return new Promise ((resolve, reject) => {
        config.api.get('/alocacao').then((res) => {
            resolve(res.data)
        }).catch((err) => {
            reject(err)
        })
    })
}

export function BuscarAlocacaoPorID(id) {
    return new Promise ((resolve, reject) => {
        config.api.get(`/alocacao/${id}`).then((res) => {
            resolve(res.data)
        }).catch((err) => {
            reject(err)
        })
    })
}
export function CriarAlocacao(Alocacao) {
    return new Promise ((resolve, reject) => {
        config.api.post(`/alocacao`, Alocacao).then((res) => {
            resolve(res.data)
        }).catch((err) => {
            reject(err)
        })
    })
}
export function AtualizarAlocacao(Alocacao) {
    return new Promise ((resolve, reject) => {
        config.api.put(`/alocacao`, Alocacao).then((res) => {
            resolve(res.data)
        }).catch((err) => {
            reject(err)
        })
    })
}

export function ExcluirAlocacaoPorID(id) {
    return new Promise ((resolve, reject) => {
        config.api.delete(`/alocacao/${id}`).then(async (res) => {
            await BuscarAlocacao()
            resolve(res.data)
        }).catch((err) => {
            reject(err)
        })
    })
}

