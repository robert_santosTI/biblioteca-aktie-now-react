import config from "../config/index";

export function CadastrarAvaliacao(Avaliacao) {
    return new Promise((resolve, reject) => {
        config.api.put('/avaliacao/publicacao', Avaliacao).then((res) => {
            resolve(res.data)
        }).catch((err) => {
            reject(err)
        })
    })
}
