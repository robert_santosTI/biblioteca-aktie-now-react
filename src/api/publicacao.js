import config from "../config/index";
import store from '../store/Index'
import { alteraPublicoes } from "../store/actions/publicacao";


export function BuscarLivros() {
    return new Promise ((resolve, reject) => {
        config.api.get('/publicacao').then((res) => {
            store.dispatch(alteraPublicoes(res.data))
            resolve(res.data)
        }).catch((err) => {
            reject(err)
        })
    })
}

export function BuscarLivrosPorID(id) {
    return new Promise ((resolve, reject) => {
        config.api.get(`/publicacao/${id}`).then((res) => {
            store.dispatch(alteraPublicoes(res.data))
            resolve(res.data)
        }).catch((err) => {
            reject(err)
        })
    })
}
export function CriarLivro(Livro) {
    return new Promise ((resolve, reject) => {
        config.api.post(`/publicacao`, Livro).then((res) => {
            resolve(res.data)
        }).catch((err) => {
            reject(err)
        })
    })
}
export function AtualizarLivro(Livro) {
    return new Promise ((resolve, reject) => {
        config.api.put(`/publicacao`, Livro).then((res) => {
            resolve(res.data)
        }).catch((err) => {
            reject(err)
        })
    })
}

export function ExcluirLivroPorID(id) {
    return new Promise ((resolve, reject) => {
        config.api.delete(`/publicacao/${id}`).then(async (res) => {
            await BuscarLivros()
            resolve(res.data)
        }).catch((err) => {
            reject(err)
        })
    })
}

